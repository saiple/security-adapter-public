package com.securityadapter.service.notification.service.util;

import com.securityadapter.model.dto.EventModel;
import com.securityadapter.model.enums.NotificationFieldEnum;
import com.securityadapter.model.regex.RegexModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.Objects;

//Класс для сборки события в строку
//Для каждого типа поля и для каждого места, куда будет отправлено сообщение,
//созданы отдельные регулярки, в которые подставляются данные.
// Эти регулярки могут быть изменены в файле resources/regexes.yaml.
@Component
public class NotificationBuilderService {

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");

    @Autowired
    //Получаем все регулярки
    private Map<NotificationFieldEnum, RegexModel> map;

    //Исходя из места назнаения, собираем сообщение
    public String getMessage(EventModel eventModel, RegexModel.Source source) {
        StringBuilder builder = new StringBuilder();
        //Если это в Лог, то еще дописываем, должно ли это сообщение отправляться в мессенжеры (Телеграмм)
        if (RegexModel.Source.LOG.equals(source)) {
            String reportableFlag = String.format("[%s] ", eventModel.getReportable() ? "REPORTABLE" : "NON-REPORTABLE");
            builder.append(reportableFlag);
        }

        //Дописываем номер события
        String eventMessage = String.format(map.get(NotificationFieldEnum.EVENT).getRegexBySource(source), eventModel.getId());
        builder.append(eventMessage).append(". ");

        //Если это не мессенджер, то дописываем еще код события
        if (isNotMessenger(source)) {
            String codeMessage =
                    String.format(map.get(NotificationFieldEnum.CODE).getRegexBySource(source), eventModel.getEvent().getCode());
            builder.append(codeMessage).append(". ");
        }

        //Берем само событие
        String mainEventMessage = eventModel.getEvent().getName();

        //Если есть зона, то записываем название зоны
        if (Objects.nonNull(eventModel.getZoneModel())) {
            mainEventMessage += ". " + eventModel.getZoneModel().getType() + " " + eventModel.getZoneModel().getPlace();

            //Если это не мессенджер, то записываем еще код зоны
            if (isNotMessenger(source)) {
                mainEventMessage += ". " +
                        String.format(map.get(NotificationFieldEnum.SHLEIF).getRegexBySource(source),
                                eventModel.getZoneModel().getShleifId());
            }
        }

        //Дописываем в строку
        String mainEventMessageFormatted =
                String.format(map.get(NotificationFieldEnum.MAIN_EVENT).getRegexBySource(source), mainEventMessage);
        builder.append(mainEventMessageFormatted).append(". ");


        if (Objects.nonNull(eventModel.getRazdelModel())) {
            //Если есть раздел, и это не мессенджер, то пишем название раздела в строку
            if (isNotMessenger(source)) {
                String razdelMessage =
                        String.format(map.get(NotificationFieldEnum.RAZDEL).getRegexBySource(source),
                                eventModel.getRazdelModel().getName());
                builder.append(razdelMessage).append(". ");
            } else {
                //Иначе пишем название раздела в мессенджер, если есть зона
                if (Objects.isNull(eventModel.getZoneModel())){
                    String razdelMessage =
                            String.format(map.get(NotificationFieldEnum.RAZDEL).getRegexBySource(source),
                                    eventModel.getRazdelModel().getName());
                    builder.append(razdelMessage).append(". ");
                }
            }
        }

        //Дописываем все остальные типы полей
        for (NotificationFieldEnum notificationFieldEnum : eventModel.getFields().keySet()) {
            String message =
                    String.format(map.get(notificationFieldEnum).getRegexBySource(source),
                            eventModel.getFields().get(notificationFieldEnum));
            builder.append(message).append(". ");
        }

        //Дописываем даты
        String dateMessage =
                String.format(map.get(NotificationFieldEnum.DATE_TIME).getRegexBySource(source),
                        eventModel.getLocalDateTime().format(FORMATTER));
        builder.append(dateMessage);

        //Выводим строку
        return builder.toString();
    }

    private boolean isNotMessenger(RegexModel.Source source){
        return RegexModel.Source.LOG.equals(source) || RegexModel.Source.DATABASE.equals(source);
    }


}
