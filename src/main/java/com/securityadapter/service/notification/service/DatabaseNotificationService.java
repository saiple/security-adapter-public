package com.securityadapter.service.notification.service;

import com.securityadapter.model.dto.EventModel;
import com.securityadapter.model.db.NotifierHistoryModel;
import com.securityadapter.model.regex.RegexModel;
import com.securityadapter.repository.NotificationHistoryRepository;
import com.securityadapter.service.notification.service.util.NotificationBuilderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class DatabaseNotificationService implements NotificationService {
    

    @Autowired
    private NotificationBuilderService notificationBuilder;

    @Autowired
    private NotificationHistoryRepository notificationHistoryRepository;

    //Сохранение в БД
    @Async
    @Override
    public void execute(EventModel eventModel){
        String message = notificationBuilder.getMessage(eventModel, RegexModel.Source.DATABASE);
        NotifierHistoryModel notifierHistoryModel = NotifierHistoryModel.builder()
                .decryptedMessage(message)
                .originalMessage(eventModel.getRaw())
                .time(LocalDateTime.now())
                .bolidId(eventModel.getId())
                .build();
        notificationHistoryRepository.save(notifierHistoryModel);
    }

}
