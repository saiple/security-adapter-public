package com.securityadapter.service.notification.service;

import com.securityadapter.model.dto.EventModel;
import com.securityadapter.model.regex.RegexModel;
import com.securityadapter.service.notification.notifier.TelegramNotifier;
import com.securityadapter.service.notification.service.util.NotificationBuilderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class LogNotificationService implements NotificationService {

    private Logger LOGGER = LoggerFactory.getLogger(TelegramNotifier.class);

    @Autowired
    private NotificationBuilderService notificationBuilder;

    //Вывод в лог
    @Async
    @Override
    public void execute(EventModel eventModel) {
        String message = notificationBuilder.getMessage(eventModel, RegexModel.Source.LOG);
        LOGGER.info(message);
    }
}
