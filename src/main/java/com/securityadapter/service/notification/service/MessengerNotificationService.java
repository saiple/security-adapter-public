package com.securityadapter.service.notification.service;

import com.securityadapter.model.dto.EventModel;
import com.securityadapter.model.db.NotifierReceiverModel;
import com.securityadapter.repository.NotificationReceiverRepository;
import com.securityadapter.service.notification.notifier.Notifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessengerNotificationService implements NotificationService  {

    @Autowired
    private NotificationReceiverRepository notificationReceiverRepository;

    @Autowired
    private ConfigurableApplicationContext context;

    //Прохождение по всем получателям и отправка им сообщений
    @Async
    @Override
    public void execute(EventModel eventModel){
        if (eventModel.getReportable()) {
            List<NotifierReceiverModel> receivers = notificationReceiverRepository.getAll();
            for (NotifierReceiverModel model : receivers) {
                if (model.getEnabled()) {
                    Notifier notifier = context.getBean(model.getReceiverType(), Notifier.class);
                    notifier.sendMessage(eventModel, model.getDestination());
                }
            }
        }
    }
}
