package com.securityadapter.service.notification.service;

import com.securityadapter.model.dto.EventModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

//Триггер для отправки событий сразу во все места
@Service
public class NotificationExecutorService {

    @Autowired
    private List<NotificationService> notificationServices;

    public void notify(EventModel eventModel) {
        for (NotificationService notificationService : notificationServices) {
            notificationService.execute(eventModel);
        }
    }

}
