package com.securityadapter.service.notification.service;

import com.securityadapter.model.dto.EventModel;

//Базовый класс
public interface NotificationService {

    void execute(EventModel eventModel);
}
