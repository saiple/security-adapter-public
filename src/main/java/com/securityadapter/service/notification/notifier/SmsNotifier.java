package com.securityadapter.service.notification.notifier;

import com.securityadapter.model.dto.EventModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

//Здесь должна быть логика отправки событий через СМС шлюз (по типу SMS.RU), но ее тут нет
@Service(Notifier.SMS)
public class SmsNotifier implements Notifier {

    private Logger LOGGER = LoggerFactory.getLogger(SmsNotifier.class);

    @Override
    public void sendMessage(EventModel eventModel, String destination) {
        LOGGER.error("Not send to {}: {}", destination, eventModel);
    }
}
