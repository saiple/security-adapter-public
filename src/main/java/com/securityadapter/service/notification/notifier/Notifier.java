package com.securityadapter.service.notification.notifier;

import com.securityadapter.model.dto.EventModel;

//Базовый класс
public interface Notifier {

    String SMS = "SMS";
    String TELEGRAM = "TELEGRAM";

    void sendMessage(EventModel eventModel, String destination);
}
