package com.securityadapter.service.notification.notifier;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.securityadapter.model.dto.EventModel;
import com.securityadapter.model.notifier.telegram.TelegramModel;
import com.securityadapter.model.regex.RegexModel;
import com.securityadapter.service.notification.service.util.NotificationBuilderService;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
//Класс для отправки сообщения в Телеграмм
@Service(Notifier.TELEGRAM)
public class TelegramNotifier implements Notifier {

    private ObjectMapper objectMapper = new ObjectMapper();
    private Logger LOGGER = LoggerFactory.getLogger(TelegramNotifier.class);

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private NotificationBuilderService notificationBuilderService;

    @Value("${notifier.telegram.token}")
    private String token;

    @Value("${notifier.telegram.host}")
    private String webhook;

    @Value("${notifier.telegram.path}")
    private String path;

    @Value("${notifier.telegram.parseMode}")
    private String parseMode;

    @Value("${notifier.telegram.method}")
    private HttpMethod httpMethod;

    //Отправить сообщение в Телеграм
    @SneakyThrows
    public void sendMessage(@NonNull EventModel eventModel, String destination) {
        try {
            //Сборка сообщения
            String message = notificationBuilderService.getMessage(eventModel, RegexModel.Source.TELEGRAM);
            //Сборка URL
            UriComponentsBuilder builder = UriComponentsBuilder
                    .newInstance()
                    .scheme("https")
                    .host(webhook)
                    .pathSegment(token, path);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

            URI uri;
            HttpEntity<MultiValueMap<String, String>> entity;
            //Сборка запроса исходя из метода
            //Я реализовал отправку используя оба метода, потому что кому-то травится такой, кому-то другой
            switch (httpMethod) {
                case GET:
                    uri = builder
                            .queryParam("text", message)
                            .queryParam("chat_id", destination)
                            .queryParam("parse_mode", parseMode)
                            .build()
                            .toUri();
                    entity = new HttpEntity<>(new LinkedMultiValueMap<>(), headers);
                    break;
                case POST:
                    uri = builder
                            .build()
                            .toUri();
                    MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
                    map.add("text", message);
                    map.add("chat_id", destination);
                    map.add("parse_mode", parseMode);
                    entity = new HttpEntity<>(map, headers);
                    break;
                default: {
                    throw new RuntimeException("Http method " + httpMethod.name() + " is not supported by Telegram");
                }
            }
            ResponseEntity<String> response = restTemplate.exchange(uri, httpMethod, entity, String.class);

        } catch (HttpClientErrorException.TooManyRequests tooManyRequests) {
            //Когда идет много запросов Телеграмму, он выдает ошибку, что идет много запросов. Также он говорит, через сколько секунд можно повторить запрос
            //Поэтому здесь реализована переотправка сообщения, если вдруг Телеграмм не принял его
            TelegramModel telegramModel = objectMapper.readValue(tooManyRequests.getResponseBodyAsString(), TelegramModel.class);
            LOGGER.warn(telegramModel.getDescription() + " Message: " + tooManyRequests.toString());
            Thread.sleep(telegramModel.getParameters().getRetryAfter());
            sendMessage(eventModel, destination);
        }
    }


}
