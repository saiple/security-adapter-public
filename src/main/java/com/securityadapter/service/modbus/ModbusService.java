package com.securityadapter.service.modbus;

import com.intelligt.modbus.jlibmodbus.master.ModbusMaster;
import com.intelligt.modbus.jlibmodbus.msg.ModbusRequestBuilder;
import com.intelligt.modbus.jlibmodbus.msg.base.ModbusRequest;
import com.intelligt.modbus.jlibmodbus.msg.response.ReadHoldingRegistersResponse;
import com.intelligt.modbus.jlibmodbus.utils.DataUtils;
import com.securityadapter.model.dto.ModbusHoldingResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

//Класс, облегачающий работу с Modbus
@Service
@ConditionalOnProperty("modbus.enabled")
public class ModbusService {

    @Autowired
    private ModbusMaster modbusMaster;

    @Value("${modbus.slaveId}")
    private Integer slaveId;

    //Используемые регистры
    @AllArgsConstructor
    @Getter
    public enum ModbusAddress {
        TIME(46165, 3),
        THE_NEWEST(46160, 1),
        THE_OLDEST(46161, 1),
        THE_OLDEST_UNREAD(46264, 14),
        EVENT_READ(46163, 1);
        int address;
        int quantity;
    }

    //Получение текущей даты из прибора
    @SneakyThrows
    public LocalDateTime getTime(){
        int[] registerValues = this.getHoldingRegisters(ModbusAddress.TIME);
        return LocalDateTime.of(registerValues[5], registerValues[4], registerValues[3], registerValues[0], registerValues[1], registerValues[2]);
    }

    //Получение самого нового события, ответ только в 10 с. ч.
    @SneakyThrows
    public int[] getTheNewest(){
        return this.getHoldingRegisters(ModbusAddress.THE_NEWEST);
    }

    //Получение самого старого события, ответ только в 10 с. ч.
    @SneakyThrows
    public int[] getTheOldest(){
        return this.getHoldingRegisters(ModbusAddress.THE_OLDEST);
    }

    //Получение самого старого непрочитанного события, ответ только в 10 с. ч.
    @SneakyThrows
    public int[] getTheOldestUnread(){
        return this.getHoldingRegisters(ModbusAddress.THE_OLDEST_UNREAD);
    }

    //Получение самого старого непрочитанного события, сырой ответ и в 10 с. ч.
    @SneakyThrows
    public ModbusHoldingResponse getTheOldestUnreadModel(){
        return this.getHoldingRegistersModel(ModbusAddress.THE_OLDEST_UNREAD);
    }

    //Отметка прочитанности собтытия
    @SneakyThrows
    public void setEventRead(int register){
        modbusMaster.writeSingleRegister(slaveId, ModbusAddress.EVENT_READ.getAddress(), register);
    }

    @SneakyThrows
    private int[] getHoldingRegisters(ModbusAddress modbusAddress){
        ModbusRequest request = ModbusRequestBuilder.getInstance().buildReadHoldingRegisters(slaveId, modbusAddress.getAddress(), modbusAddress.getQuantity());
        ReadHoldingRegistersResponse response = (ReadHoldingRegistersResponse) modbusMaster.processRequest(request);
        return parseResponse(response.getBytes()).getResponse();
    }

    @SneakyThrows
    private ModbusHoldingResponse getHoldingRegistersModel(ModbusAddress modbusAddress){
        ModbusRequest request = ModbusRequestBuilder.getInstance().buildReadHoldingRegisters(slaveId, modbusAddress.getAddress(), modbusAddress.getQuantity());
        ReadHoldingRegistersResponse response = (ReadHoldingRegistersResponse) modbusMaster.processRequest(request);
        return parseResponse(response.getBytes());
    }

    private ModbusHoldingResponse parseResponse(byte[] registerValues) {
        int[] ints = new int[registerValues.length];
        String[] raws = new String[registerValues.length];
        for (int i = 0; i < registerValues.length; i++){
            String raw = DataUtils.toAscii(registerValues[i]);
            raws[i] = raw;
            ints[i] = Integer.parseInt(raw,16);
        }
        return ModbusHoldingResponse
                .builder()
                .raw(raws)
                .response(ints)
                .build();
    }

    public static String hexBytesToString(byte[] registerValues) {
        return DataUtils.toAscii(registerValues);
    }





}
