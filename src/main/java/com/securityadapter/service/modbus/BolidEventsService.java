package com.securityadapter.service.modbus;

import com.securityadapter.model.db.ListedEventModel;
import com.securityadapter.model.db.RazdelModel;
import com.securityadapter.model.db.ZoneModel;
import com.securityadapter.model.dto.EventModel;
import com.securityadapter.model.dto.ModbusHoldingResponse;
import com.securityadapter.model.enums.AdditionalFieldCodeType;
import com.securityadapter.model.enums.NotificationFieldEnum;
import com.securityadapter.repository.ListedEventRepository;
import com.securityadapter.repository.RazdelRepository;
import com.securityadapter.repository.ZoneRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

//Основая логика получения событий
@Service
@ConditionalOnProperty("modbus.enabled")
public class BolidEventsService {

    private Logger LOGGER = LoggerFactory.getLogger(BolidEventsService.class);

    @Autowired
    private ModbusService modbusService;

    @Autowired
    private ListedEventRepository listedEventRepository;

    @Autowired
    private ZoneRepository zoneRepository;

    @Autowired
    private RazdelRepository razdelRepository;

    // Метод по получению событий события из С2000-ПП
    public Optional<EventModel> getEvents() {
        // Получаем самое старое событие
        int[] theOldest = modbusService.getTheOldest();
        // Получаем самое новое событие
        int[] theNewest = modbusService.getTheNewest();
        // Находим разницу
        int result = getSumAllArray(theOldest) + getSumAllArray(theNewest);
        if (result != 0) {
            // Получаем самое старое непрочитанное событие
            // Получаем сырой и переведенный в 10 с. ч. ответ
            ModbusHoldingResponse theOldestUnread = modbusService.getTheOldestUnreadModel();
            String raw = theOldestUnread.getAsString();

            //Выводим в лог
            LOGGER.info("Received frame: {}", raw);

            //Oтвет, переведенный в 10 с. ч.
            int[] response = theOldestUnread.getResponse();

            //Если в ответе одни нули, то выводим пустоту
            if (!isNotAllZero(response)) {
                return Optional.empty();
            }

            //Номер события
            int id = calculateHighAndLowBytes(response[0], response[1]);

            //Код события
            int code = response[3];

            //Достаем событие из БД
            Optional<ListedEventModel> eventCandidate = listedEventRepository.getEventByCode(code);

            //Если такого события нет в БД, генерируем свое событие
            ListedEventModel event = eventCandidate.orElseGet(() -> ListedEventModel.builder()
                    .code(code)
                    .name(String.format("Illegal code: %d. Frame: %s", code, raw))
                    .enabled(true)
                    .build());

            //Информационные поля, кроме даты
            Map<NotificationFieldEnum, String> additionalFields = new HashMap<>();

            //Текущее время
            LocalDateTime localDateTime = LocalDateTime.now();
            //Зона
            ZoneModel zoneModel = null;
            //Раздел
            RazdelModel razdelModel = null;

            int i = 4;
            while (i < response.length) {
                //Если байт 0, то пропускаем дальше
                if (response[i] == 0) {
                    i++;
                    // Все типы полей описаны в AdditionalFieldCodeType
                } else if (response[i] == AdditionalFieldCodeType.DATE_TIME.getCode()) { //Обработка парсинга даты и времени
                    //Сдвигаем каретку на информационные байты
                    i += 2;
                    //Копируем в массив информационные байты даты и времени
                    int[] date = Arrays.copyOfRange(response, i, i + AdditionalFieldCodeType.DATE_TIME.getLength());
                    try {
                        //Создаем дату из байтов
                        localDateTime = LocalDateTime.of(date[5] + 2000, date[4], date[3], date[0], date[1], date[2]);
                    } catch (DateTimeException e) {
                        //Иногда дата приходила в формате по типу 255.255.255 255:255:255 (точно не помню), поэтому я оставляю текущее время с кода
                        String dateString = date[0] + ":" + date[1] + ":" + date[2] + " " + date[3] + "." + date[4] + "." + (date[5] + 2000);
                        //Выводим ошибку в лог
                        LOGGER.error("Unable parse date: {}", dateString);
                    }
                    // Сдвигаем картетку на другое поле на длину текущего поля (6)
                    i += AdditionalFieldCodeType.DATE_TIME.getLength();
                } else if (response[i] == AdditionalFieldCodeType.ZONE.getCode()) { //Обработка парсинга зоны
                    //Сдвигаем каретку на информационные байты
                    i += 2;
                    //Считаем номер зоны
                    int value = calculateHighAndLowBytes(response[i], response[i + 1]);
                    //Достаем зону из БД по номеру
                    Optional<ZoneModel> zoneModelCandidate = zoneRepository.getZoneById(value);
                    // Если зона есть, то она записывается в соответствующую дата-модель
                    // В сообщении в последущем будет выведено название зоны
                    if (zoneModelCandidate.isPresent()) {
                        zoneModel = zoneModelCandidate.get();
                    } else {
                        // Если нет, то будет выведен номер зоны
                        additionalFields.put(NotificationFieldEnum.ZONE, String.valueOf(value));
                    }
                    // Сдвигаем картетку на другое поле на длину текущего поля (2)
                    i += AdditionalFieldCodeType.ZONE.getLength();
                } else if (response[i] == AdditionalFieldCodeType.RAZDEL.getCode()) { //Обработка парсинга раздела
                    //Сдвигаем каретку на информационные байты
                    i += 2;
                    //Считаем номер раздела
                    int value = calculateHighAndLowBytes(response[i], response[i + 1]);
                    //Достаем раздел из БД по номеру
                    Optional<RazdelModel> razdelModelCandidate = razdelRepository.getRazdelById(value);
                    // Если раздел есть, то она записывается в соответствующую дата-модель
                    // В сообщении в последущем будет выведено название раздела
                    if (razdelModelCandidate.isPresent()) {
                        razdelModel = razdelModelCandidate.get();
                    } else {
                        // Если нет, то будет выведен номер раздела
                        additionalFields.put(NotificationFieldEnum.RAZDEL, String.valueOf(value));
                    }
                    // Сдвигаем картетку на другое поле на длину текущего поля (2)
                    i += AdditionalFieldCodeType.RAZDEL.getLength();
                } else {
                    //Обработка парсинга остальных типов полей
                    Optional<AdditionalFieldCodeType> fieldCodeTypeCandidate = AdditionalFieldCodeType.getTypeByCode(response[i]);
                    if (fieldCodeTypeCandidate.isPresent()) {
                        //Сдвигаем каретку на информационные байты
                        i += 2;
                        //Считаем данные
                        int value = calculateHighAndLowBytes(response[i], response[i + 1]);
                        //Сохраняем данные
                        additionalFields.put(fieldCodeTypeCandidate.get().getFieldEnum(), String.valueOf(value));
                        // Сдвигаем картетку на другое поле на длину текущего поля
                        i += fieldCodeTypeCandidate.get().getLength();
                    } else {
                        //Если не смогли распознать тип поля, берем код поля
                        int fieldCode = response[i];
                        //Длину поля
                        int fieldLength = response[++i];
                        // Сдвигаем картетку на другое поле на длину текущего поля
                        i += fieldLength;
                        //Выводим ошибку в лог
                        LOGGER.error("Illegal additional parameter. Code {}. Length {}", fieldCode, fieldLength);
                    }
                }
            }

            //Собираем дату-модель события
            EventModel eventModel = EventModel.builder()
                    .id(id)
                    .reportable(event.getEnabled())
                    .event(event)
                    .raw(raw)
                    .zoneModel(zoneModel)
                    .razdelModel(razdelModel)
                    .localDateTime(localDateTime)
                    .fields(additionalFields)
                    .build();
            //Отмечаем, что событие прочитано
            modbusService.setEventRead(id);
            //Возвращаем событие
            return Optional.of(eventModel);
        }
        //Если событий нет новых, возвращаем пустоту
        return Optional.empty();
    }

    private int getSumAllArray(int[] oldest) {
        int res = 0;
        for (int step : oldest)
            res += step;
        return res;
    }

    private LocalDateTime getEventDate(int[] bytes) {
        int[] date = new int[6];
        int iterator = 0;
        for (int i = bytes.length - 1; i >= 0; i--) {
            if (iterator >= 6) {
                break;
            }
            if (bytes[i] != 0) {
                date[iterator] = bytes[i];
                iterator++;
            }
        }
        return LocalDateTime.of(date[0] + 2000, date[1], date[2], date[5], date[4], date[3]);
    }

    private boolean isNotAllZero(int[] bytes) {
        boolean result = false;
        for (int byt : bytes) {
            result |= (byt != 0);
        }
        return result;
    }

    private int calculateHighAndLowBytes(int high, int low) {
        return high * 256 + low;
    }


}
