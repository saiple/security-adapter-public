package com.securityadapter.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Optional;

//Типы полей, которые возвращает С2000-ПП
@AllArgsConstructor
@Getter
public enum AdditionalFieldCodeType {
    USER(1, 2, NotificationFieldEnum.USER),
    RAZDEL(2, 2, NotificationFieldEnum.RAZDEL),
    ZONE(3, 2, NotificationFieldEnum.ZONE),
    RELAY_NUMBER(5, 2, NotificationFieldEnum.RELAY_NUMBER),
    RELAY_STATUS(7, 2, NotificationFieldEnum.RELAY_STATUS),
    DATE_TIME(11, 6, NotificationFieldEnum.DATE_TIME),
    RAZDEL_ID(24, 2, NotificationFieldEnum.RAZDEL_ID);

    private final int code;
    private final int length;
    private final NotificationFieldEnum fieldEnum;

    public static Optional<AdditionalFieldCodeType> getTypeByCode(int code) {
        for (AdditionalFieldCodeType type : values())
            if (type.getCode() == code)
                return Optional.of(type);
        return Optional.empty();
    }

}
