package com.securityadapter.model.enums;

//Типы полей, которые формируются во время парсинга ответа
//И эти поля отправляются в БД, Телеграмм и т.д.
public enum NotificationFieldEnum {
    EVENT,
    USER,
    RAZDEL,
    ZONE,
    RELAY_NUMBER,
    RELAY_STATUS,
    DATE_TIME,
    RAZDEL_ID,
    MAIN_EVENT,
    CODE,
    SHLEIF

}
