package com.securityadapter.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

//Дата-модель ответа с Modbus
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ModbusHoldingResponse {
    //Переведенные в 10 с. ч.
    private int[] response;
    //Сырые байты
    private String[] raw;

    public String getAsString() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < raw.length; i++) {
            builder.append(raw[i]);
            if (i != raw.length - 1)
                builder.append(".");
        }
        return builder.toString();
    }
}
