package com.securityadapter.model.dto;

import com.securityadapter.model.db.ListedEventModel;
import com.securityadapter.model.db.RazdelModel;
import com.securityadapter.model.db.ZoneModel;
import com.securityadapter.model.enums.NotificationFieldEnum;
import lombok.*;
import org.springframework.lang.Nullable;

import java.time.LocalDateTime;
import java.util.Map;

//Дата-модель самого события
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ToString
public class EventModel {
    private Integer id;
    private ListedEventModel event;
    private String raw;
    private Boolean reportable;
    @Nullable private ZoneModel zoneModel;
    @Nullable private RazdelModel razdelModel;
    private LocalDateTime localDateTime;
    private Map<NotificationFieldEnum, String> fields;
}
