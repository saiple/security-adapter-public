package com.securityadapter.model.notifier.telegram;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

//Дата-модель, которую возвращает Телеграмм, если много запросов
@NoArgsConstructor
@Builder
@Data
@AllArgsConstructor
public class TelegramModel {
    private String ok;

    @JsonAlias("error_code")
    private String errorCode;

    private String description;

    private Parameters parameters;

    @NoArgsConstructor
    @Builder
    @Data
    @AllArgsConstructor
    public static class Parameters {
        @JsonAlias("retry_after")
        private Integer retryAfter;
    }
}
