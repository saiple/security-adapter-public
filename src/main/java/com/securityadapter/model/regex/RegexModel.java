package com.securityadapter.model.regex;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

//Регулярка, исходя из типа поля
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class RegexModel {

    public enum Source {
        TELEGRAM, SMS, DATABASE, LOG
    }

    private String telegram;
    private String sms;
    private String database;
    private String log;

    public String getRegexBySource(Source source){
        switch (source){
            case TELEGRAM:
                return getTelegram();
            case SMS:
                return getSms();
            case DATABASE:
                return getDatabase();
            case LOG:
                return getLog();
            default:
                throw new IllegalArgumentException("Illegal source " + source.name());
        }
    }
}
