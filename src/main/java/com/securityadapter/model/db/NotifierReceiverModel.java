package com.securityadapter.model.db;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

//Дата модель одной записи в таблице получателей
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class NotifierReceiverModel {
    private Long id;
    private String destination;
    private String receiverType;
    private Boolean enabled;
}
