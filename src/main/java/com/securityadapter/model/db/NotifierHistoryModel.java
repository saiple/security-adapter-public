package com.securityadapter.model.db;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

//Дата модель одной записи в таблице истории событий
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class NotifierHistoryModel {
    private Integer bolidId;
    private String originalMessage;
    private String decryptedMessage;
    private LocalDateTime time;
}
