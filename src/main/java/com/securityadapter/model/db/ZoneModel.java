package com.securityadapter.model.db;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

//Дата модель одной записи в таблице зон
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class ZoneModel {
    private Integer zoneId;
    private Integer shleifId;
    private String place;
    private String type;
    private String device;

}
