package com.securityadapter.model.db;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

//Дата модель одной записи в таблице разделов
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class RazdelModel {
    public Integer id;
    private String name;
    private String description;
}
