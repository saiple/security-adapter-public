package com.securityadapter.model.db;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

//Дата модель одной записи в таблице событий
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ListedEventModel {
    private Integer code;
    private String name;
    private String description;
    private Integer priority;
    private Boolean enabled;
}
