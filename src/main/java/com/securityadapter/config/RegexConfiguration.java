package com.securityadapter.config;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.securityadapter.model.enums.NotificationFieldEnum;
import com.securityadapter.model.regex.RegexModel;
import lombok.SneakyThrows;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.util.HashMap;
import java.util.Map;

//Иницализация регулярок
@Configuration
public class RegexConfiguration {

    private ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
    private static final String REGEXES_FILE = "regexes.yaml";

    @SneakyThrows
    @Bean
    public Map<NotificationFieldEnum, RegexModel> regexes(){
        Resource resource = new ClassPathResource(REGEXES_FILE);
        Map<NotificationFieldEnum, RegexModel> map = objectMapper.readValue(resource.getInputStream(),
                new TypeReference<HashMap<NotificationFieldEnum, RegexModel>>() {});
        return map;
    }

}
