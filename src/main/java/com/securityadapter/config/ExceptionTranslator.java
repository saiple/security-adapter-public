package com.securityadapter.config;

import org.jooq.ExecuteContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DefaultExecuteListener;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.support.SQLErrorCodeSQLExceptionTranslator;
import org.springframework.jdbc.support.SQLExceptionTranslator;
import org.springframework.jdbc.support.SQLStateSQLExceptionTranslator;

import java.util.Objects;

//Иницализация для работы с БД
public class ExceptionTranslator extends DefaultExecuteListener {

  @Override
  public void exception(final ExecuteContext context) {
    if (Objects.nonNull(context.sqlException())) {
      SQLDialect dialect = context.dialect();
      SQLExceptionTranslator translator = (dialect != null)
              ? new SQLErrorCodeSQLExceptionTranslator(dialect.thirdParty().springDbName())
              : new SQLStateSQLExceptionTranslator();

      DataAccessException jooqException = translator.translate("jOOQ", context.sql(), context.sqlException());

      context.exception(jooqException);
    }
  }

}