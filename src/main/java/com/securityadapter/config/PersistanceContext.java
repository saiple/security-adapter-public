package com.securityadapter.config;

import lombok.Data;
import org.jooq.SQLDialect;
import org.jooq.impl.DataSourceConnectionProvider;
import org.jooq.impl.DefaultConfiguration;
import org.jooq.impl.DefaultDSLContext;
import org.jooq.impl.DefaultExecuteListenerProvider;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.sqlite.SQLiteDataSource;

import javax.sql.DataSource;

//Иницализация для работы с БД
@Configuration
@ComponentScan("com.securityadapter")
@EnableTransactionManagement
@ConfigurationProperties(prefix = "spring.datasource")
@Data
public class PersistanceContext {

  private String dialect;
  private String url;

  @Bean
  public DataSource dataSource(){
    SQLiteDataSource dataSource = new SQLiteDataSource();
    dataSource.setUrl(url);
    return dataSource;
  }

  @Bean
  public TransactionAwareDataSourceProxy transactionAwareDataSource() {
    return new TransactionAwareDataSourceProxy(this.dataSource());
  }

  @Bean
  public DataSourceTransactionManager transactionManager() {
    return new DataSourceTransactionManager(this.dataSource());
  }

  @Bean
  public DataSourceConnectionProvider connectionProvider() {
    return new DataSourceConnectionProvider(this.transactionAwareDataSource());
  }

  @Bean
  public ExceptionTranslator exceptionTransformer() {
    return new ExceptionTranslator();
  }

  @Bean
  public DefaultDSLContext dsl() {
    return new DefaultDSLContext(this.configuration());
  }

  @Bean
  public DefaultConfiguration configuration() {
    final DefaultConfiguration jooqConfiguration = new DefaultConfiguration();
    jooqConfiguration.set(this.connectionProvider());
    jooqConfiguration.set(new DefaultExecuteListenerProvider(this.exceptionTransformer()));

    final SQLDialect sqlDialect = SQLDialect.valueOf(dialect);
    jooqConfiguration.set(sqlDialect);

    return jooqConfiguration;
  }
}
