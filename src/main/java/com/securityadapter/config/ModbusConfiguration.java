package com.securityadapter.config;

import com.intelligt.modbus.jlibmodbus.exception.ModbusIOException;
import com.intelligt.modbus.jlibmodbus.master.ModbusMaster;
import com.intelligt.modbus.jlibmodbus.master.ModbusMasterFactory;
import com.intelligt.modbus.jlibmodbus.serial.*;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//Иницализация для работы с Modbus
@Configuration
@ConfigurationProperties(value = "modbus")
@Data
public class ModbusConfiguration {

    private String device;
    private Integer baudRate;
    private Integer dataBits;
    private String parity;
    private Integer stopBits;

    private Logger LOGGER = LoggerFactory.getLogger(ModbusConfiguration.class);

    @Bean
    @ConditionalOnProperty("modbus.enabled")
    public ModbusMaster modbusMaster() throws SerialPortException, ModbusIOException {
        SerialUtils.setSerialPortFactory(new SerialPortFactoryPJC());

        LOGGER.info("Using port {}", device);

        SerialParameters sp = new SerialParameters();
     //   Modbus.setLogLevel(Modbus.LogLevel.LEVEL_DEBUG);

        sp.setDevice(device);
        sp.setBaudRate(SerialPort.BaudRate.getBaudRate(baudRate));
        sp.setDataBits(dataBits);
        sp.setParity(SerialPort.Parity.valueOf(parity));
        sp.setStopBits(stopBits);

        ModbusMaster modbus = ModbusMasterFactory.createModbusMasterRTU(sp);
        modbus.connect();
        return modbus;
    }



}
