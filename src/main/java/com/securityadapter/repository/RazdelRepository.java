package com.securityadapter.repository;

import com.securityadapter.jooq.tables.Razdel;
import com.securityadapter.model.db.RazdelModel;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Optional;

//Класс по работе с таблицей разделов
@Repository
public class RazdelRepository {

    @Autowired
    private DSLContext dslContext;

    private Razdel RAZDEL = Razdel.RAZDEL;

    public Optional<RazdelModel> getRazdelById(Integer id){
        return dslContext
                .select()
                .from(RAZDEL)
                .where(RAZDEL.ID.eq(id))
                .fetchOptionalInto(RazdelModel.class);
    }

}
