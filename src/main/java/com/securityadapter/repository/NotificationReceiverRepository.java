package com.securityadapter.repository;

import com.securityadapter.jooq.tables.NotificationReceiver;
import com.securityadapter.model.db.NotifierReceiverModel;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

//Класс по работе с таблицей получателей
@Repository
public class NotificationReceiverRepository {

    @Autowired
    private DSLContext dsl;

    private final NotificationReceiver NOTIFICATION_RECEIVER_TABLE = NotificationReceiver.NOTIFICATION_RECEIVER;

    public List<NotifierReceiverModel> getAll(){
        return this.dsl
                .select()
                .from(this.NOTIFICATION_RECEIVER_TABLE)
                .fetchInto(NotifierReceiverModel.class);
    }

    public List<NotifierReceiverModel> getById(Long id){
        return this.dsl
                .select()
                .from(this.NOTIFICATION_RECEIVER_TABLE)
                .where(NOTIFICATION_RECEIVER_TABLE.ID.eq(id))
                .fetchInto(NotifierReceiverModel.class);
    }


    public Optional<NotifierReceiverModel> update(NotifierReceiverModel notifierReceiverModel){
        return this.dsl
                .update(NOTIFICATION_RECEIVER_TABLE)
                .set(NOTIFICATION_RECEIVER_TABLE.DESTINATION, notifierReceiverModel.getDestination())
                .set(NOTIFICATION_RECEIVER_TABLE.RECEIVER_TYPE, notifierReceiverModel.getReceiverType())
                .set(NOTIFICATION_RECEIVER_TABLE.ENABLED, notifierReceiverModel.getEnabled())
                .where(NOTIFICATION_RECEIVER_TABLE.ID.eq(notifierReceiverModel.getId()))
                .returning(NOTIFICATION_RECEIVER_TABLE.ID, NOTIFICATION_RECEIVER_TABLE.DESTINATION, NOTIFICATION_RECEIVER_TABLE.RECEIVER_TYPE, NOTIFICATION_RECEIVER_TABLE.ENABLED)
                .fetchOptional()
                .map(r -> r.into(NOTIFICATION_RECEIVER_TABLE).into(NotifierReceiverModel.class));
    }

    public void delete(Long id){
        dsl.delete(NOTIFICATION_RECEIVER_TABLE)
            .where(NOTIFICATION_RECEIVER_TABLE.ID.eq(id))
            .execute();
    }

    public NotifierReceiverModel save(NotifierReceiverModel notifierReceiverModel){
        return dsl.insertInto(NOTIFICATION_RECEIVER_TABLE)
            .set(NOTIFICATION_RECEIVER_TABLE.DESTINATION, notifierReceiverModel.getDestination())
            .set(NOTIFICATION_RECEIVER_TABLE.RECEIVER_TYPE, notifierReceiverModel.getReceiverType())
            .returning(NOTIFICATION_RECEIVER_TABLE.ID, NOTIFICATION_RECEIVER_TABLE.DESTINATION, NOTIFICATION_RECEIVER_TABLE.RECEIVER_TYPE)
            .fetchOne()
            .map(r -> r.into(NOTIFICATION_RECEIVER_TABLE).into(NotifierReceiverModel.class));
    }


}
