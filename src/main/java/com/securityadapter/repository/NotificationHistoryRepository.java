package com.securityadapter.repository;

import com.securityadapter.jooq.tables.NotificationHistory;
import com.securityadapter.model.db.NotifierHistoryModel;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

//Класс по работе с таблицей историей событий
@Repository
public class NotificationHistoryRepository {

    @Autowired
    private DSLContext dsl;

    private final NotificationHistory NOTIFICATION_HISTORY_TABLE = NotificationHistory.NOTIFICATION_HISTORY;

    public List<NotifierHistoryModel> getAll(){
        return this.dsl
                .select()
                .from(this.NOTIFICATION_HISTORY_TABLE)
                .fetchInto(NotifierHistoryModel.class);
    }


    public List<NotifierHistoryModel> getByBolidId(Integer id){
        return this.dsl
                .select()
                .from(this.NOTIFICATION_HISTORY_TABLE)
                .where(NOTIFICATION_HISTORY_TABLE.BOLID_ID.eq(id))
                .fetchInto(NotifierHistoryModel.class);
    }


    public NotifierHistoryModel save(NotifierHistoryModel notifierHistoryModel){
        return dsl.insertInto(NOTIFICATION_HISTORY_TABLE)
                .set(NOTIFICATION_HISTORY_TABLE.DECRYPTED_MESSAGE, notifierHistoryModel.getDecryptedMessage())
                .set(NOTIFICATION_HISTORY_TABLE.ORIGINAL_MESSAGE, notifierHistoryModel.getOriginalMessage())
                .set(NOTIFICATION_HISTORY_TABLE.TIME, notifierHistoryModel.getTime())
                .set(NOTIFICATION_HISTORY_TABLE.BOLID_ID, notifierHistoryModel.getBolidId())
                .returning(NOTIFICATION_HISTORY_TABLE.DECRYPTED_MESSAGE, NOTIFICATION_HISTORY_TABLE.ORIGINAL_MESSAGE, NOTIFICATION_HISTORY_TABLE.TIME, NOTIFICATION_HISTORY_TABLE.BOLID_ID)
                .fetchOne()
                .map(r -> r.into(NOTIFICATION_HISTORY_TABLE).into(NotifierHistoryModel.class));
    }

}
