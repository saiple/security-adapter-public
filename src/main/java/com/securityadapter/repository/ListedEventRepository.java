package com.securityadapter.repository;

import com.securityadapter.jooq.tables.Event;
import com.securityadapter.model.db.ListedEventModel;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Optional;

//Класс по работе с таблицей событий
@Repository
public class ListedEventRepository {

    @Autowired
    private DSLContext dslContext;

    private Event EVENT = Event.EVENT;

    public Optional<ListedEventModel> getEventByCode(Integer code){
        return dslContext
                .select()
                .from(EVENT)
                .where(EVENT.CODE.eq(code))
                .fetchOptionalInto(ListedEventModel.class);
    }
}
