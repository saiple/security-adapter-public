package com.securityadapter.repository;

import com.securityadapter.jooq.tables.Zone;
import com.securityadapter.model.db.ZoneModel;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Optional;

//Класс по работе с таблицей зон
@Repository
public class ZoneRepository {

    @Autowired
    private DSLContext dslContext;

    private Zone ZONE = Zone.ZONE;

    public Optional<ZoneModel> getZoneById(Integer id){
        return dslContext
                .select()
                .from(ZONE)
                .where(ZONE.ZONE_ID.eq(id))
                .fetchOptionalInto(ZoneModel.class);
    }

}
