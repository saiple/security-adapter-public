package com.securityadapter.app;

import com.securityadapter.model.dto.EventModel;
import com.securityadapter.service.modbus.BolidEventsService;
import com.securityadapter.service.notification.service.NotificationExecutorService;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;
import java.util.Optional;

//Главный класс
@SpringBootApplication
@ComponentScan("com.securityadapter")
@EnableAsync
public class SecurityAdapterApplication {

    @Autowired
    private NotificationExecutorService notificationExecutorService;

    @Autowired(required = false)
    private BolidEventsService bolidEventsService;

    @Value("${modbus.pollDelay}")
    private Double pollDelay;

    @Value("${modbus.enabled}")
    private Boolean modbusEnabled;

    public static void main(String[] args) {
        ConfigurableApplicationContext context =
                SpringApplication.run(SecurityAdapterApplication.class, args);
    }

    //Бесконечный цикла опроса
    @EventListener(ApplicationReadyEvent.class)
    @SneakyThrows
    public void run() {
        while (Objects.nonNull(bolidEventsService) && modbusEnabled){
            Optional<EventModel> eventCandidate = bolidEventsService.getEvents();
            eventCandidate.ifPresent(event -> notificationExecutorService.notify(event));
            //Задержка по времени
            Thread.sleep((long)(pollDelay * 1000));
        }
    }

    @Bean
    public RestTemplate httpClient() {
        return new RestTemplate();
    }

}
